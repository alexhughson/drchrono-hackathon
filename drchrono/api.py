from datetime import datetime
import requests
from requests.exceptions import HTTPError
import json

from django.conf import settings
from django.core.cache import cache

from social_auth_drchrono.backends import drchronoOAuth2

API_HOST = 'https://drchrono.com'


class TooManyAuthsException(Exception):
    pass

class NotAuthenticatedException(Exception):
    pass


def handle_token_expiry(fn):
    def ret(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except HTTPError as e:
            if e.response.status_code == 401 and e.response.content == 'Authorization failed.':
                # TODO - Refresh the access token
                raise NotAuthenticatedException('Somehow you are logged out of the API')
    return ret

def cache_api(key_string):
    def decorator(fn):
        def returned_function(*args, **kwargs):
            cached_value = cache.get(key_string.format(*args, **kwargs))
            if cached_value is not None:
                return json.loads(cached_value)

            ret_value = fn(*args, **kwargs)
            ret_json = json.dumps(ret_value)
            cache.set(key_string.format(*args, **kwargs), ret_json)
            return ret_value
        return returned_function
    return decorator

def _get_user_social_account(user):
    auths = user.social_auth.all()
    if len(auths) == 1:
        return auths[0]
    elif len(auths) == 0:
        return None
    else:
        raise TooManyAuthsException('Seriously, you are logged in as too many people')


def _get_auth_header(user):
    social_auth = _get_user_social_account(user)
    auth_header = drchronoOAuth2.get_auth_header(social_auth.access_token)
    return auth_header

#untested
def _return_paginated_api_response(api_response, headers):
    all_data = []
    while True:
        response_json = api_response.json()

        all_data += response_json['results']
        if response_json['next'] is not None and len(response_json['next']) > 1:
            api_response = requests.get(
                results['next'],
                headers=headers,
            )
        else:
            return all_data


def revoke_token(user):
    API_PATH = '/o/revoke_token/'

    social_user = _get_user_social_account(user)
    resp = requests.post(
        API_HOST + API_PATH,
        data={
            'token': social_user.access_token,
            'client_id': settings.SOCIAL_AUTH_DRCHRONO_KEY,
            'client_secret': settings.SOCIAL_AUTH_DRCHRONO_SECRET
        })

    return resp


def am_i_authenticated(user):
    try:
        get_doctor_details(user)
        return True
    except NotAuthenticatedException:
        return False


@handle_token_expiry
def get_doctor_details(user):
    user_social = _get_user_social_account(user)
    API_PATH = '/api/doctors/{}'.format(user_social.uid)

    auth_header = _get_auth_header(user)

    resp = requests.get(
        API_HOST + API_PATH,
        headers=auth_header,
    )
    resp.raise_for_status()
    return resp.json()


@handle_token_expiry
@cache_api('get_patient_{1}')
def get_patient(user, patient_id):
    API_PATH = '/api/patients/{}'.format(patient_id)
    auth_header = _get_auth_header(user)
    resp = requests.get(
        API_HOST + API_PATH,
        headers=auth_header,
    )
    resp.raise_for_status()

    return resp.json()


@handle_token_expiry
def get_appointments_today(user):
    API_PATH = '/api/appointments'

    today = datetime.now().strftime("%Y-%m-%d")

    social_auth = _get_user_social_account(user)
    doctor_id = social_auth.uid

    args = {
        'date': today,
        'doctor': doctor_id,
    }

    auth_header = _get_auth_header(user)

    resp = requests.get(
        API_HOST + API_PATH,
        params=args,
        headers=auth_header,
    )
    resp.raise_for_status()
    return _return_paginated_api_response(resp, auth_header)
    #return resp.json()['results']


@handle_token_expiry
def search_for_patient(user, first_name='',
                       last_name='', ssn=''):

    API_PATH = '/api/patients'

    args = {}
    if len(first_name) > 0:
        args['first_name'] = first_name
    if len(last_name) > 0:
        args['last_name'] = last_name

    auth_header = _get_auth_header(user)

    resp = requests.get(
        API_HOST + API_PATH,
        params=args,
        headers=auth_header,
    )
    resp.raise_for_status()
    patient_list = _return_paginated_api_response(resp, auth_header)
    #patient_list = resp.json()['results']

    if len(ssn) > 0:
        ret = [patient for patients in patient_list if patient['social_security_number'] == ssn]
    else:
        ret = patient_list

    return ret

@handle_token_expiry
def find_appointments_today_for_patient(user, patient_id):
    API_PATH = '/api/appointments'

    today = datetime.now().strftime('%Y-%m-%d')
    social_auth = _get_user_social_account(user)

    args = {
        'date': today,
        'doctor': social_auth.uid,
        'patient': patient_id,
    }
    auth_header = _get_auth_header(user)

    resp = requests.get(
        API_HOST + API_PATH,
        params=args,
        headers=auth_header,
    )
    resp.raise_for_status()
    return _return_paginated_api_response(resp, auth_header)
    #return resp.json()['results']

@handle_token_expiry
def update_patient_chart(user, patient_id, patient_chart):
    API_PATH = '/api/patients/{}'.format(patient_id)

    args = patient_chart

    auth_header = _get_auth_header(user)
    resp = requests.patch(
        API_HOST + API_PATH,
        data=args,
        headers=auth_header,
    )
    resp.raise_for_status()

@handle_token_expiry
def get_appointment_data(user, appointment_id):
    API_PATH = '/api/appointments/{}'.format(appointment_id)
    auth_header = _get_auth_header(user)
    resp = requests.get(
        API_HOST + API_PATH,
        headers=auth_header,
    )
    resp.raise_for_status()
    return resp.json()

@handle_token_expiry
def _mark_appointment_as(user, appointment_id, status):
    API_PATH = '/api/appointments/{}'.format(appointment_id)

    args = {
        'status': status,
    }

    auth_header = _get_auth_header(user)
    resp = requests.patch(
        API_HOST + API_PATH,
        data=args,
        headers=auth_header,
    )
    resp.raise_for_status()


def mark_appointment_as_arrived(user, appointment_id):
    _mark_appointment_as(user, appointment_id, 'Arrived')


def mark_appointment_as_started(user, appointment_id):
    _mark_appointment_as(user, appointment_id, 'In Session')


# Deprecado
def get_custom_demographics(user):
    API_PATH = '/api/custom_demographics'
    auth_header = _get_auth_header(user)
    social_user = _get_user_social_account(user)
    params = {
        'doctor': social_user.uid,
    }

    resp = requests.get(
        API_HOST + API_PATH,
        params=params,
        headers=auth_header,
    )
    resp.raise_for_status()
    return _return_paginated_api_response(resp, auth_header)
    return resp.json()['results']


from datetime import datetime


from models import AppointmentCheckin

def save_appointment_check_in(appointment_id, appointment_date):
    already_checked_in = AppointmentCheckin.objects.filter(
        appointment_id=appointment_id
    ).exists()

    if already_checked_in:
        return

    AppointmentCheckin.objects.create(
        appointment_id=appointment_id,
        check_in_time=datetime.now(),
        appointment_time=appointment_date, # Needs to be fixed
    )

def start_appointment(appointment_id):
    AppointmentCheckin.objects.filter(
        appointment_id=appointment_id,
        started_at=None,
    ).update(started_at=datetime.now())

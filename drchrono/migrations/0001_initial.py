# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AppointmentCheckin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('appointment_id', models.IntegerField()),
                ('check_in_time', models.DateTimeField()),
                ('appointment_time', models.DateTimeField()),
            ],
        ),
    ]

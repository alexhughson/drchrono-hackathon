from django.conf import settings
from django.db import connection, models

# Create your models here.

class AppointmentCheckin(models.Model):
    appointment_id = models.IntegerField()
    check_in_time = models.DateTimeField()

    # Denormalized to calculate waiting time
    appointment_time = models.DateTimeField()
    started_at = models.DateTimeField(null=True)


def get_average_wait_time():
    assert settings.DATABASES['default']['ENGINE'] == 'django.db.backends.sqlite3'

    with connection.cursor() as cursor:
        average_wait = cursor.execute("""
            SELECT round(AVG(
                (julianday(started_at) - julianday(check_in_time)) * 24 * 60
            ), 0) AS average_wait_minutes
            FROM drchrono_appointmentcheckin
            WHERE
                check_in_time IS NOT NULL
                AND started_at IS NOT NULL
        """)
        query_result = average_wait.fetchone()
        return query_result[0]

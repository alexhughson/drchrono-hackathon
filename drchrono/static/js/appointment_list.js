$(document).ready(function () {
    run_clock_update();
});

var ONE_HOUR_IN_MS = 60 * 60 * 1000
var ONE_MINUTE_IN_MS = 60 * 1000

function run_clock_update() {
    update_clocks();
    window.setTimeout(
        run_clock_update,
        ONE_MINUTE_IN_MS
    );
}

function update_clocks() {
    var $checked_in_appts = $('tr.checked_in')
    $checked_in_appts.each(function (index, element) {
        var $el = $(element)
        var $time_waiting_el = $el.find('.time_waiting')
        // Python is using seconds and js is using MS
        var check_in_ts = $time_waiting_el.attr('data-checkin-time') * 1000
        var start_time = $time_waiting_el.attr('data-start-time');
        var waiting_until;
        if (typeof start_time !== typeof undefined && start_time !== false) {
            waiting_until = start_time * 1000;
        } else {
            waiting_until = (new Date()).getTime();
        }

        var diff = waiting_until - check_in_ts;
        var hours = Math.floor(diff / ONE_HOUR_IN_MS)
        var minutes = Math.floor((diff % ONE_HOUR_IN_MS) / ONE_MINUTE_IN_MS)

        var timestr = minutes.toString() + ' Minutes'
        if (hours > 0) {
            timestr = hours.toString() + ' Hours, ' + timestr
        }

        $time_waiting_el.html(timestr)
    });
}
from django import template

register = template.Library()


@register.inclusion_tag('tags/simple_form_field.html')
def simple_form_field(field_name, field_description, input_type, value=''):

    return {
        'field_name': field_name,
        'field_description': field_description,
        'input_type': input_type,
        'value': value,
    }

from datetime import datetime, timedelta
from dateutil import parser
import pytz

from django.test import TestCase, RequestFactory
from mock import patch, MagicMock, DEFAULT

from models import AppointmentCheckin

from drchrono.views import (
    _appointment_list_data,
    appointment_list,
    complete_check_in,
)

# Create your tests here.

MOCK_TS = '2014-02-24T15:32:19'
MOCK_TS_DATETIME = pytz.utc.localize(
    parser.parse(MOCK_TS)
)


def mock_api(fn):
    def ret(*args, **kwargs):
        with patch.multiple(
            'drchrono.views', api=DEFAULT,
        ) as patch_content:
            api = patch_content['api']

            api.get_appointments_today.return_value = [
            {
                'id': u'1',
                'patient': u'2',
                'scheduled_time': MOCK_TS,
            },
            {
                'id': u'2',
                'patient': u'2',
                'scheduled_time': MOCK_TS,
            },
            {
                'id': u'3',
                'patient': u'3',
                'scheduled_time': MOCK_TS,
            }]
            api.get_patient.return_value = {}
            api.get_appointment_data.return_value = {
                'id': u'1',
                'scheduled_time': MOCK_TS,
            }
            api.mark_appointment_as_started = MagicMock()
            fn(*args, **kwargs)
    return ret

class ChronoHackTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()


    @mock_api
    def test_appointment_list(self):
        AppointmentCheckin.objects.create(
            appointment_id=1,
            check_in_time=datetime.now(),
            appointment_time=datetime.now(),
        )


        view_data = _appointment_list_data(MagicMock())
        self.assertTrue(view_data[0]['checked_in'])
        self.assertFalse(view_data[1]['checked_in'])

    @mock_api
    def test_appointment_started(self):
        AppointmentCheckin.objects.create(
            appointment_id=1,
            check_in_time=datetime.now(),
            appointment_time=datetime.now(),
            started_at=datetime.now(),
        )
        AppointmentCheckin.objects.create(
            appointment_id=2,
            check_in_time=datetime.now(),
            appointment_time=datetime.now(),
        )

        view_data = _appointment_list_data(MagicMock())
        self.assertTrue(view_data[0]['started'])
        self.assertFalse(view_data[1]['started'])
        self.assertFalse(view_data[2]['started'])

    @mock_api
    def test_cant_start_multiple_times(self):
        five_minutes_ago = datetime.now(tz=pytz.utc) - timedelta(minutes=5)
        request = self.factory.post(
            '/appointment_list',
            {
                'appointment_id': 1
            },
        )
        request.user = MagicMock()

        AppointmentCheckin.objects.create(
            appointment_id=1,
            check_in_time=five_minutes_ago,
            appointment_time=five_minutes_ago,
            started_at=five_minutes_ago,
        )

        from views import _start_appointment
        _start_appointment(request.user, 1)

        check_in = AppointmentCheckin.objects.get(appointment_id=1)

        self.assertEquals(check_in.started_at, five_minutes_ago)

    @mock_api
    def test_cant_checkin_multiple_times(self):
        request = self.factory.post(
            '/complete_check_in',
            {
                'patient_id': 1,
                'appointment_id': 1,
            }
        )
        request.user = MagicMock()

        complete_check_in(request)
        complete_check_in(request)
        checkins = AppointmentCheckin.objects.filter(
            appointment_id=1,
        ).count()
        self.assertEqual(checkins, 1)

    @mock_api
    def test_checkin_does_not_move(self):
        five_minutes_ago = datetime.now(tz=pytz.utc) - timedelta(minutes=5)
        AppointmentCheckin.objects.create(
            appointment_id=1,
            check_in_time=five_minutes_ago,
            appointment_time=five_minutes_ago,
        )
        request = self.factory.post(
            '/complete_check_in',
            {
                'patient_id': 1,
                'appointment_id': 1,
            }
        )
        request.user = MagicMock()

        complete_check_in(request)

        checkin = AppointmentCheckin.objects.get(
            appointment_id=1,
        )
        self.assertEqual(checkin.check_in_time, five_minutes_ago)

    @mock_api
    def test_checkin_saves_appt_time(self):
        from views import complete_check_in
        request = self.factory.post(
            '/complete_check_in',{
                'patient_id': 1,
                'appointment_id': 1,
            },
        )
        request.user = MagicMock()

        complete_check_in(request)

        checkin = AppointmentCheckin.objects.get(appointment_id=1)
        self.assertEqual(
            checkin.appointment_time,
            MOCK_TS_DATETIME,
        )

    def test_average_wait_time(self):
        from models import get_average_wait_time
        dt_now = datetime.now()
        dt_an_hour_ago = dt_now - timedelta(hours=1)
        dt_two_hours_ago = dt_now - timedelta(hours=2)
        self.assertIsNone(get_average_wait_time())

        AppointmentCheckin.objects.create(
            appointment_id=1,
            appointment_time=dt_now,
            check_in_time=dt_now,
        )
        self.assertIsNone(get_average_wait_time())

        AppointmentCheckin.objects.create(
            appointment_id=2,
            appointment_time=dt_now,

            check_in_time=dt_an_hour_ago,
            started_at=dt_now,
        )
        self.assertEqual(get_average_wait_time(), 60)
        AppointmentCheckin.objects.create(
            appointment_id=3,
            appointment_time=dt_now,

            check_in_time=dt_two_hours_ago,
            started_at=dt_now,
        )
        self.assertEqual(get_average_wait_time(), 90)

        AppointmentCheckin.objects.create(
            appointment_id=4,
            appointment_time=dt_now,
            check_in_time=dt_two_hours_ago,
            started_at=dt_an_hour_ago,
        )
        self.assertEqual(get_average_wait_time(), 80)

    def test_start_appointment(self):
        dt_now = datetime.now()
        dt_hour_ago = datetime.now() - timedelta(hours=1)
        AppointmentCheckin.objects.create(
            appointment_id=1,
            check_in_time=dt_hour_ago,
            appointment_time=dt_now,
        )
        request = self.factory.post(
            '/appointment_list',
            {
                'appointment_id': 1
            }
        )
        request.user = MagicMock()

        resp = appointment_list(request)
        checkin = AppointmenCheckin.objects.get(
            appointment_id=1
        )
        self.assertIsNotNone(checkin.started_at)
    def test_cache(self):
        from django.core.cache import cache
        cache.set('keyvalue', 'hey')
        self.assertEqual(cache.get('keyvalue'), 'hey')











from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.contrib import admin
import views


urlpatterns = [
    url(r'^$', views.index),
    url(r'^index', views.index),
    url(r'^api_logged_out', TemplateView.as_view(template_name='api_logged_out.html')),
    url(r'^whatup*', views.index),
    url(r'^appointment_list', views.appointment_list),
    url(r'^check_in', views.check_in),
    url(r'^patient_search_result', views.patient_info),
    url(r'^complete_check_in', views.complete_check_in),
    url(r'', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^revoke_token', views.revoke_token),
]

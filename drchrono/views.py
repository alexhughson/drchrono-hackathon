# Create your views here.
import json
from dateutil import parser
from datetime import datetime

from django.shortcuts import render, redirect
from django.http import HttpResponse


import api

import control

from models import (
    AppointmentCheckin,
    get_average_wait_time,
)

GENDER_LIST = ['Other', 'Female', 'Male']

CHART_FIELDS = [
    'gender',
    'date_of_birth',
    'email',
    'address',
    'city',
    'cell_phone',
    'emergency_contact_name',
    'emergency_contact_phone',
    'emergency_contact_relation',
    'employer',
]

def handle_api_logout(fn):
    def ret(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except api.NotAuthenticatedException:
            return redirect('/api_logged_out')
    return ret


def index(request):
    is_logged_in = api.am_i_authenticated(request.user)
    return render(
        request,
        'index.html',
        {
            'is_logged_in': is_logged_in,
        }
    )

def check_in(request):
    return render(
        request,
        'check_in.html',
    )

def revoke_token(request):
    ret = api.revoke_token(request.user)
    try:
        ret.raise_for_status()
        return HttpResponse('deactivated')
    except:
        return HttpResponse('something went wrong')


@handle_api_logout
def appointment_list(request):
    if request.method == 'POST':
        _start_appointment(
            request.user,
            request.POST.get('appointment_id', None),
        )

    appointment_data = _appointment_list_data(request)
    average_wait = get_average_wait_time()


    return render(
        request,
        'appointment_list.html',
        {
            'data': appointment_data,
            'average_wait': average_wait,
        },
    )

def _appointment_list_data(request):
    appts = api.get_appointments_today(request.user)
    appointment_ids = set()
    patient_ids = set()

    for appointment in appts:
        _clean_appointment_for_display(appointment)

        appointment_ids.add(appointment['id'])
        patient_ids.add(appointment['patient'])


    checkin_records = AppointmentCheckin.objects.filter(
        appointment_id__in=appointment_ids,
    )

    checkin_map = {rec.appointment_id: rec for rec in checkin_records}

    patient_map = {
        patient_id: api.get_patient(request.user, patient_id)
        for patient_id in patient_ids
    }

    appointment_data = [
        {
            'checkin': checkin_map.get(appt['id'], None),
            'patient': patient_map[appt['patient']],
            'appointment': appt,
            'checked_in': checkin_map.get(appt['id'], None) is not None,
            'started': (
                checkin_map.get(appt['id'], None) is not None
                and checkin_map[appt['id']].started_at is not None
            ),
        } for appt in appts
    ]
    return appointment_data


def _clean_appointment_for_display(appointment):
    appointment['scheduled_time'] = parser.parse(
        appointment['scheduled_time']
    ).time()
    appointment['patient'] = int(appointment['patient'])
    appointment['id'] = int(appointment['id'])
    return appointment


# note, bad user experience, they just entered their data
@handle_api_logout
def patient_info(request):
    first_name = request.POST.get('first_name', '')
    last_name = request.POST.get('last_name', '')
    ssn = request.POST.get('ssn', '')

    patient_list = api.search_for_patient(
        request.user,
        first_name=first_name,
        last_name=last_name,
        ssn=ssn,
    )

    assert len(patient_list) == 1

    patient = patient_list[0]

    patient_appointments = api.find_appointments_today_for_patient(
        request.user,
        patient['id'],
    )

    assert len(patient_appointments) > 0

    appointment = _clean_appointment_for_display(
        patient_appointments[0]
    )

    return render(
        request,
        'chart_update.html',
        {
            'patient': patient,
            'appointment': appointment,
            'gender_list': GENDER_LIST,
        },
    )


def _parse_patient_chart_form(request):
    ret = {}
    for field in CHART_FIELDS:
        ret[field] = request.POST.get(field, None)
    return ret


def _start_appointment(user, appointment_id):
    assert appointment_id is not None
    api.mark_appointment_as_started(user, appointment_id)
    control.start_appointment(appointment_id)


@handle_api_logout
def complete_check_in(request):
    patient_id = request.POST.get('patient_id', None)
    appointment_id = request.POST.get('appointment_id', None)

    chart_data = _parse_patient_chart_form(request)

    api.update_patient_chart(
        request.user,
        patient_id,
        chart_data,
    )

    api.mark_appointment_as_arrived(request.user, appointment_id)
    appointment_data = api.get_appointment_data(request.user, appointment_id)
    control.save_appointment_check_in(
        appointment_id,
        appointment_data['scheduled_time'],
    )

    return render(
        request,
        'check_in_complete.html',
    )



